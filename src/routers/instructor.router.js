const express = require('express');
const { instructorMiddleware, parseQueryParams } = require('../middleware');
const { instructorController } = require('../controllers');

const instructorRouter = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     NewInstructor:
 *       type: object
 *       properties:
 *         first_name:
 *           type: string
 *           example: 'Bob'
 *         last_name:
 *           type: string
 *           example: 'Jones'
 *         birth_date:
 *           type: string
 *           description: The instructor's birth date, formatted as YYYY-MM-DD.
 *           example: '1965-09-23'
 *         hired_date:
 *           type: string
 *           description: The date when the instructor was hired, formatted as YYYY-MM-DD.
 *           example: '2015-03-11'
 *     Instructor:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: An ineteger ID that is unique to this instructor record.
 *               example: 42
 *         - $ref: '#/components/schemas/NewInstructor'
 */

/**
 * @swagger
 * /instructors:
 *   get:
 *     tags:
 *       - instructors
 *     summary: Retrieves all instructor records.
 *     responses:
 *       200:
 *         description: A list of all hired instructors.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Instructor'
 */
instructorRouter.get('/', instructorController.getInstructors);

/**
 * @swagger
 * /instructors/{instructorId}:
 *   get:
 *     tags:
 *       - instructors
 *     summary: Retrieves a single instructor record.
 *     parameters:
 *       - in: path
 *         name: instructorId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the instructor to retrieve.
 *     responses:
 *       200:
 *         description: The instructor record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Instructor'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
instructorRouter.get(
  '/:instructorId',
  parseQueryParams,
  instructorMiddleware.validateGetInstructorByIdReq,
  instructorController.getInstructorById
);

/**
 * @swagger
 * /instructors:
 *   post:
 *     tags:
 *       - instructors
 *     summary: Creates a new instructor record.
 *     requestBody:
 *       description: The information to create a new instructor record.
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewInstructor'
 *     responses:
 *       201:
 *         description: The new instructor record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Instructor'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
instructorRouter.post(
  '/',
  parseQueryParams,
  instructorMiddleware.validateCreateInstructorReq,
  instructorController.createInstructor
);

/**
 * @swagger
 * /instructors/{instructorId}:
 *   put:
 *     tags:
 *       - instructors
 *     summary: Updates an existing instructor record.
 *     parameters:
 *       - in: path
 *         name: instructorId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the instructor record to be updated.
 *     requestBody:
 *       description: The information to update the instructor record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewInstructor'
 *     responses:
 *       200:
 *         description: The updated instructor record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Instructor'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
instructorRouter.put(
  '/:instructorId',
  parseQueryParams,
  instructorMiddleware.validateUpdateInstructorReq,
  instructorController.updateInstructorById
);

/**
 * @swagger
 * /instructors/{instructorId}:
 *   delete:
 *     tags:
 *       - instructors
 *     summary: Deletes an existing instructor record.
 *     parameters:
 *       - in: path
 *         name: instructorId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the instructor record to be deleted.
 *     responses:
 *       200:
 *         description: Confirmation that the instructor record was successfully deleted.
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
instructorRouter.delete(
  '/:instructorId',
  parseQueryParams,
  instructorMiddleware.validateDeleteInstructorReq,
  instructorController.deleteInstructorById
);

module.exports = instructorRouter;
