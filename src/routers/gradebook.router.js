const express = require('express');
const { gradebookMiddleware, parseQueryParams } = require('../middleware');
const { gradebookController } = require('../controllers');

const gradebookRouter = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     NewGrade:
 *       type: object
 *       properties:
 *         student_id:
 *           type: integer
 *           description: The integer ID of the student.
 *           example: 84
 *         course_id:
 *           type: integer
 *           description: The integer ID of the course.
 *           example: 2
 *         assignment_name:
 *           type: string
 *           description: The name of the course assignment.
 *           example: 'Unit 1 Individual Project'
 *         assignment_due_date:
 *           type: string
 *           description: The due date of the assignment, formatted as YYYY-MM-DD.
 *           example: '2021-12-14'
 *         points_possible:
 *           type: integer
 *           description: The total amount of points that can be earned for this assignment.
 *           example: 150
 *         points_earned:
 *           type: integer
 *           description: The total amount of points the student has earned for this assignment.
 *           example: 147
 *     Grade:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: An integer ID unique to this gradebook record.
 *               example: 124
 *         - $ref: '#/components/schemas/NewGrade'
 */

/**
 * @swagger
 * /gradebook/{gradeId}:
 *   get:
 *     tags:
 *       - gradebook
 *     summary: Retrieves a single gradebook record.
 *     parameters:
 *       - in: path
 *         name: gradeId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the gradebook record to retrieve.
 *     responses:
 *       200:
 *         description: The gradebook record.
 *         content: 
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Grade'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
gradebookRouter.get(
  '/:gradeId',
  parseQueryParams,
  gradebookMiddleware.validateGetGradeByIdReq,
  gradebookController.getGradeById
);

/**
 * @swagger
 * /gradebook:
 *   post:
 *     tags:
 *       - gradebook
 *     summary: Creates a gradebook record for a student.
 *     requestBody:
 *       description: The information to create a new gradebook record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewGrade'
 *     responses:
 *       201:
 *         description: The new gradebook record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Grade'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
gradebookRouter.post(
  '/',
  gradebookMiddleware.validateCreateGradeReq,
  gradebookController.createGrade
);

/**
 * @swagger
 * /gradebook/{gradeId}:
 *   put:
 *     tags:
 *       - gradebook
 *     summary: Updates an existing gradebook record.
 *     parameters:
 *       - in: path
 *         name: gradeId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The ID of the gradebook record to be updated.
 *     requestBody:
 *       description: The information to update a gradebook record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewGrade'
 *     responses:
 *       200:
 *         description: The updated gradebook record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Grade'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
gradebookRouter.put(
  '/:gradeId',
  parseQueryParams,
  gradebookMiddleware.validateUpdateGradeReq,
  gradebookController.updateGradeById
);

/**
 * @swagger
 * /gradebook/{gradeId}:
 *   delete:
 *     tags:
 *       - gradebook
 *     summary: Deletes an existing gradebook record.
 *     parameters:
 *       - in: path
 *         name: gradeId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The ID of the gradebook record to be updated.
 *     responses:
 *       200:
 *         description: Confirmation that the gradebook record was successfully deleted.
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
gradebookRouter.delete(
  '/:gradeId',
  parseQueryParams,
  gradebookMiddleware.validateDeleteGradeReq,
  gradebookController.deleteGradeById
);

module.exports = gradebookRouter;
