const { db } = require('../db');
const { sqlTimestamp } = require('../utils');

async function getInstructors(_, res) {
  const instructors = await db('instructors')
    .select('id', 'first_name', 'last_name', 'birth_date', 'hired_date');
  return res.status(200).json(instructors);
}

async function getInstructorById(req, res) {
  const { instructorId } = req.params;
  const result = await db('instructors')
    .where({ id: instructorId })
    .select('id', 'first_name', 'last_name', 'birth_date', 'hired_date')
    .first();

  if (!result)
    return res.status(400).json({ errors: [`Instructor with ID ${instructorId} does not exist.`] });
  return res.status(200).json(result);
}

async function createInstructor(req, res) {
  const ts = sqlTimestamp();
  const [newId] = await db('instructors')
    .insert({ ...req.body, created_at: ts, updated_at: ts });

  return res.status(201).json({ id: newId, ...req.body });
}

async function updateInstructorById(req, res) {
  const { instructorId } = req.params;

  const ts = sqlTimestamp();
  await db('instructors')
    .where({ id: instructorId })
    .update({ ...req.body, updated_at: ts });

  return res.status(200).json({ id: instructorId, ...req.body });
}

async function deleteInstructorById(req, res) {
  const instructorId = req.params.instructorId;
  await db('instructors').where({ id: instructorId }).delete();
  res.sendStatus(200);
}

module.exports = {
  getInstructors,
  getInstructorById,
  createInstructor,
  updateInstructorById,
  deleteInstructorById,
};
