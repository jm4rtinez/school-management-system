const { makeParamsValidation, validate } = require('./utils');

/** @type { import('./utils').JSONSchema7 } */
const enrollmentDtoSchema = {
  type: 'object',
  required: ['student_id', 'course_id'],
  properties: {
    student_id: { type: 'number', multipleOf: 1 },
    course_id: { type: 'number', multipleOf: 1 },
  },
  additionalProperties: false,
};

const validateGetEnrollmentByIdReq = validate({
  params: makeParamsValidation('enrollmentId'),
});

const validateCreateEnrollmentReq = validate({ body: enrollmentDtoSchema });

module.exports = {
  validateGetEnrollmentByIdReq,
  validateCreateEnrollmentReq,
};
