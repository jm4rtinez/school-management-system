const { validate, makeParamsValidation } = require('./utils');

/** @type { import('./utils').JSONSchema7 } */
const gradeDtoSchema = {
  type: 'object',
  required: ['student_id', 'course_id', 'assignment_name', 'assignment_due_date', 'points_possible', 'points_earned'],
  properties: {
    student_id: { type: 'number', multipleOf: 1 },
    course_id: { type: 'number', multipleOf: 1 },
    assignment_name: { type: 'string', minLength: 1, maxLength: 255 },
    assignment_due_date: { type: 'string', format: 'date' },
    points_possible: { type: 'number' },
    points_earned: { type: 'number' },
  },
  additionalProperties: false,
};

const validateGetGradesReq = validate({ params: makeParamsValidation('studentId') });

const validateGetGradeByIdReq = validate({ params: makeParamsValidation('gradeId') });

const validateCreateGradeReq = validate({ body: gradeDtoSchema });

const validateUpdateGradeReq = validate({
  params: makeParamsValidation('gradeId'),
  body: gradeDtoSchema,
});

const validateDeleteGradeReq = validate({ params: makeParamsValidation('gradeId') });

module.exports = {
  validateGetGradesReq,
  validateGetGradeByIdReq,
  validateCreateGradeReq,
  validateUpdateGradeReq,
  validateDeleteGradeReq,
};
