const courseMiddleware = require('./course.middleware');
const enrollmentMiddleware = require('./enrollment.middleware');
const gradebookMiddleware = require('./gradebook.middleware');
const instructorMiddleware = require('./instructor.middleware');
const { parseQueryParams } = require('./parseQueryParams');
const studentMiddleware = require('./student.middleware');

module.exports = {
  courseMiddleware,
  enrollmentMiddleware,
  gradebookMiddleware,
  instructorMiddleware,
  parseQueryParams,
  studentMiddleware,
};
