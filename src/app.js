const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');

const { swaggerSpec } = require('./docs');
const { ValidationError } = require('express-json-validator-middleware');

const {
  courseRouter,
  enrollmentRouter,
  gradebookRouter,
  instructorRouter,
  studentRouter
} = require('./routers');

const app = express();

app.use(cors());
app.use(express.json());

app.use('/courses', courseRouter);
app.use('/enrollments', enrollmentRouter);
app.use('/gradebook', gradebookRouter);
app.use('/instructors', instructorRouter);
app.use('/students', studentRouter);

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.route('*').all((_, res) => res.sendStatus(404));

app.use((err, _, res, next) => {
  if (err instanceof ValidationError) {
    const errors = Object
      .entries(err.validationErrors)
      .flatMap(([key, ajvErrs]) => {
        return ajvErrs.map(({ keyword, dataPath, message }) => {
          const prefix = `Error in request ${key}`;
          const m = message.length ? `${message}` : 'is invalid';

          if (keyword === 'required') {
            return `${prefix}:${dataPath.length && ' ' + dataPath || ''} ${m}.`;
          }

          return `${prefix}: property '${dataPath.slice(1)}' ${m}.`;
        });
      });
    res.status(400).json({ errors });
    next();
  } else {
    next(err);
  }
});

app.use((err, _, res, next) => {
  res.sendStatus(500);
  next(err);
});

module.exports = { app };