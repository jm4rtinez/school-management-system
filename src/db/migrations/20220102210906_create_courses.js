exports.up = function (knex) {
  return knex.schema.createTable('courses', function (table) {
    table.increments('id');
    table.string('identifier', 255).notNullable();
    table.string('name', 255).notNullable();
    table.text('description').notNullable();
    table.date('start_date').notNullable();
    table.date('end_date').notNullable();
    table.integer('capacity');
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('courses');
};
