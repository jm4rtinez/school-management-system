const instructorsData = require('./seed-data/instructors.json');
const { sqlTimestamp } = require('../../utils');

exports.seed = async function (knex) {
  await knex('instructors').del();
  await knex('sqlite_sequence').delete().where({ name: 'instructors' });

  const ts = sqlTimestamp();
  await knex('instructors')
    .insert(instructorsData.map(d => ({ ...d, created_at: ts, updated_at: ts })));
};
