const studentsData = require('./seed-data/students.json');
const { sqlTimestamp } = require('../../utils');

exports.seed = async function (knex) {
  await knex('students').del();
  await knex('sqlite_sequence').delete().where({ name: 'students' });

  const ts = sqlTimestamp();
  await knex('students')
    .insert(studentsData.map(d => ({ ...d, created_at: ts, updated_at: ts })));
};
