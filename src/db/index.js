/** @type { import('../../node_modules/knex/types/index').knex }  */
// @ts-ignore
const knex = require('knex');
const config = require('./knexfile');

/** @type { import('../../node_modules/knex/types/index').Knex } */
let db;
if (process.env.NODE_ENV === 'test') {
  db = knex(config.test);
} else {
  db = knex(config.development);
}

module.exports = { db };