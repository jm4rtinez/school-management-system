function sqlTimestamp() {
  return new Date().toISOString().slice(0, 19);
}

module.exports = {
  sqlTimestamp,
};
