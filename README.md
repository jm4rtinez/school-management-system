# School Management System

## Getting started

Install dependencies:

```sh
npm install
```

Bring the test database up to date with the latest migrations:

```sh
npm run migrate
```

Seed the test database:

```sh
npm run seed
```

Start the application:

```sh
node index.js <PORT>
```

Or start the development server:

```sh
npm start
```

## Documentation

API documentation can be found at the `/docs` route.


