const supertest = require('supertest');
const { app } = require('../src/app');

const request = supertest(app);

describe('Instructors', function () {
  describe('GET /instructors/:instructorId', function () {
    it('returns an error if the instructor does not exist', function () {
      const expected = {
        errors: ['Instructor with ID 999 does not exist.']
      };

      return request.get('/instructors/999').expect(400, expected);
    });
  });

  describe('POST /instructors', function () {
    it('inserts a new instructor record', function () {
      const data = {
        first_name: 'Foo',
        last_name: 'Bar',
        birth_date: '2022-01-15',
        hired_date: '2022-02-22',
      };

      const expected = { id: 11, ...data };

      return request
        .post('/instructors')
        .send(data)
        .expect(201, expected);
    });
  });

  describe('PUT /instructors/:instructorId', function () {
    it('updates an existing course record', function () {
      const data = {
        first_name: 'Foo',
        last_name: 'Bar',
        birth_date: '2022-01-15',
        hired_date: '2022-02-22',
      };

      const expected = { id: 1, ...data };

      return request
        .put('/instructors/1')
        .send(data)
        .expect(200, expected);
    });
  });

  describe('DELETE /instructors/:instructorId', function () {
    it('deletes an existing course record', function () {
      return request
        .delete('/instructors/1')
        .expect(200);
    });
  });
});
