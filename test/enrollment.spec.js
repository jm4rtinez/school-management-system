const supertest = require('supertest');
const { app } = require('../src/app');

const request = supertest(app);

describe('Enrollments', function () {
  describe('GET /enrollments/:enrollmentId', function () {
    it('returns an error if the enrollment does not exist', function () {
      const expected = {
        errors: ['Enrollment with ID 999 does not exist.']
      };

      return request.get('/enrollments/999').expect(400, expected);
    });
  });

  describe('POST /enrollments', function () {
    it('returns an error if the course is at capacity', function () {
      const data = {
        student_id: 1,
        course_id: 1,
      };

      const expected = {
        errors: ['Course with ID 1 is at capacity (20).'],
      };

      return request
        .post('/enrollments')
        .send(data)
        .expect(400, expected);
    });

    it('returns an error if a student is already enrolled in the course', function () {
      const data = {
        student_id: 1,
        course_id: 2,
      };

      const expected = {
        errors: ['Student with ID 1 is already enrolled in course with ID 2'],
      };

      return request
        .post('/enrollments')
        .send(data)
        .expect(400, expected);
    });

    it('adds a new enrollment record if the course is not at capacity', function () {
      const data = {
        student_id: 1,
        course_id: 3,
      };

      const expected = {
        id: 81,
        student_id: 1,
        course_id: 3,
      };

      return request
        .post('/enrollments')
        .send(data)
        .expect(201, expected);
    });
  });
});
