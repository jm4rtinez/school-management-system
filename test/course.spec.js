const supertest = require('supertest');
const { app } = require('../src/app');

const request = supertest(app);

describe('Courses', function () {
  describe('GET /courses/:courseId', function () {
    it('returns an error if the course does not exist', function () {
      const expected = {
        errors: ['Course with ID 999 does not exist.']
      };

      return request.get('/courses/999').expect(400, expected);
    });
  });

  describe('POST /courses', function () {
    it('inserts a new course record', function () {
      const data = {
        identifier: 'CS000-1111A-22',
        name: 'Test Course',
        description: 'Test description.',
        start_date: '2022-01-15',
        end_date: '2022-02-22',
        capacity: 18,
      };

      const expected = { id: 6, ...data };

      return request
        .post('/courses')
        .send(data)
        .expect(201, expected);
    });
  });

  describe('PUT /courses/:courseId', function () {
    it('updates an existing course record', function () {
      const data = {
        identifier: 'CS000-1111A-22',
        name: 'Test Course',
        description: 'Test description.',
        start_date: '2022-01-15',
        end_date: '2022-02-22',
        capacity: 18,
      };

      const expected = { id: 1, ...data };

      return request
        .put('/courses/1')
        .send(data)
        .expect(200, expected);
    });
  });

  describe('DELETE /courses/:courseId', function () {
    it('deletes an existing course record', function () {
      return request
        .delete('/courses/1')
        .expect(200);
    });
  });
});

