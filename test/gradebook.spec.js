const supertest = require('supertest');
const { app } = require('../src/app');

const request = supertest(app);

describe('Gradebook', function () {
  describe('GET /gradebook/:gradeId', function () {
    it('returns an error if the grade does not exist', function () {
      const expected = {
        errors: ['Grade with ID 999 does not exist.']
      };

      return request.get('/gradebook/999').expect(400, expected);
    });
  });

  describe('POST /gradebook', function () {
    it('returns an error if the student already has a grade for the assignment entered', function () {
      const data = {
        student_id: 1,
        course_id: 2,
        assignment_name: 'Week 1 Discussion Board',
        assignment_due_date: '2021-09-21',
        points_possible: 100,
        points_earned: 90,
      };

      const expected = {
        errors: [`Student with ID 1 already has a grade for assignment: Week 1 Discussion Board`],
      };

      return request
        .post('/gradebook')
        .send(data)
        .expect(400, expected);
    });

    it('inserts a new gradebook record', function () {
      const data = {
        student_id: 1,
        course_id: 2,
        assignment_name: 'Week 5 Discussion Board',
        assignment_due_date: '2021-10-19',
        points_possible: 100,
        points_earned: 90,
      };

      const expected = { id: 641, ...data };

      return request
        .post('/gradebook')
        .send(data)
        .expect(201, expected);
    });
  });

  describe('PUT /gradebook/:gradeId', function () {
    it('updates an existing gradebook record', function () {
      const data = {
        student_id: 1,
        course_id: 2,
        assignment_name: 'Week 1 Discussion Board',
        assignment_due_date: '2021-09-21',
        points_possible: 100,
        points_earned: 90,
      };

      const expected = { id: 1, ...data };

      return request
        .put('/gradebook/1')
        .send(data)
        .expect(200, expected);
    });
  });

  describe('DELETE /gradebook/:gradeId', function () {
    it('deletes an existing gradebook record', function () {
      return request
        .delete('/gradebook/1')
        .expect(200);
    });
  });
});
